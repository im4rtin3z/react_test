import React from 'react';

const Paragraph = props => {
    
    const style = {
        width: '40%'
      }

    return (
        <div  style={style}>
            <p>
                {props.username}
            </p>
            <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                 sed do eiusmod tempor incididunt ut labore et dolore magna
                  aliqua. Ut enim ad minim veniam, quis nostrud
                   exercitation ullamco laboris nisi ut aliquip ex
                    ea commodo consequat.
            </p>
        </div>
    )
}

export default Paragraph;
