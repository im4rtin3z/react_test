import React, { useState } from 'react';
import './App.css';
import UserInput from "./Components/UserInput";
import UserOutput from "./Components/UserOutput";

function App() {

  const [stateUserName,setStateUsername] = useState({
    username:"imartinez"
  });

  const changeNameHandler = (event) =>{
    setStateUsername({
      username: event.target.value
    });
  }

  const style = {
    height : '20%',
    width: '20%'
  }
  
  return (
    <div className="App">
      <header className="App-header">
        <UserInput changed={changeNameHandler}  username={stateUserName.username} />
        <UserOutput style={style} username={stateUserName.username}/>
        <UserOutput />
      </header>
    </div>
  );
}

export default App;
