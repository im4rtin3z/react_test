import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actionTypes from '../store/actions';

import Person from '../components/Person/Person';
import AddPerson from '../components/AddPerson/AddPerson';

class Persons extends Component {
    state = {
        persons: []
    }

    personAddedHandler = (name, age) => {
        const newPerson = {
            id: Math.random(), // not really unique but good enough here!
            name: 'Max',
            age: Math.floor(Math.random() * 40)
        }

        this.props.onPersonAdded(newPerson, name, age);
    }

    personDeletedHandler = (personId) => {
        this.props.onPersonDeleted(personId);
    }

    render() {
        return (
            <div>
                <AddPerson personAdded={this.personAddedHandler} />
                {this.props.localPersons.map(person => (
                    <Person
                        key={person.id}
                        name={person.name}
                        age={person.age}
                        clicked={() => this.personDeletedHandler(person.id)} />
                ))}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        localPersons: state.persons
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onPersonAdded: (newPerson, name, age) => dispatch({ type: actionTypes.ADD_PERSON, person: newPerson, name: name, age: age }),
        onPersonDeleted: (personId) => dispatch({ type: actionTypes.DELETE_PERSON, id: personId }),
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(Persons);