import * as actionTypes from './actions';

const initialState = {
    persons: []
}

const reducer = (state = initialState, action) => {

    switch (action.type) {
        case actionTypes.ADD_PERSON:
            return {
                persons: state.persons.concat({
                    ...action.person,
                    name: action.name,
                    age: action.age
                })
            }

        case actionTypes.DELETE_PERSON:
            return {
                persons: state.persons.filter(person => person.id !== action.id)
            }
    }

    return state;
}

export default reducer;