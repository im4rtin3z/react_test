
import React, { Component } from 'react';
import './App.css';
import ValidationComponent from './Components/ValidationComponent'
import CharComponent from './Components/CharComponent'


class App extends Component {

  state = {
    lengthInputCounter: 0,
    characters: [],
    inputText:"",
  }

  inputCounter = (event) => {

    const charactersFromInput = event.target.value.split('');
    const stateToUpdate = [...this.state];

    stateToUpdate.lengthInputCounter = event.target.value.length;
    stateToUpdate.characters = charactersFromInput;

    this.setState({
      lengthInputCounter: stateToUpdate.lengthInputCounter,
      characters: charactersFromInput,
      inputText: event.target.value
    });

  }

  clickOnCharacter = (characterIndex) => {

    const charactersToUpdate = [...this.state.characters];
    charactersToUpdate.splice(characterIndex, 1);

    this.setState({
      characters: charactersToUpdate,
      inputText: charactersToUpdate.join('')
    });

  }

  render() {
    const listOfCharacters = (
      <div>
        {
          this.state.characters.map((character, index) => {
            return <CharComponent
              value={character}
              key={index}
              click={() => this.clickOnCharacter(index)}
            />
          })
        }
      </div>
    );

    return (
      <div className="App">
        <ol>
          <li>Create an input field (in App component) with a change listener which outputs the length of the entered text below it (e.g. in a paragraph).</li>
          <li>Create a new component (=> ValidationComponent) which receives the text length as a prop</li>
          <li>Inside the ValidationComponent, either output "Text too short" or "Text long enough" depending on the text length (e.g. take 5 as a minimum length)</li>
          <li>Create another component (=> CharComponent) and style it as an inline box (=> display: inline-block, padding: 16px, text-align: center, margin: 16px, border: 1px solid black).</li>
          <li>Render a list of CharComponents where each CharComponent receives a different letter of the entered text (in the initial input field) as a prop.</li>
          <li>When you click a CharComponent, it should be removed from the entered text.</li>
        </ol>
        <p>Hint: Keep in mind that JavaScript strings are basically arrays!</p>
        <hr />
        <div>
          <label htmlFor="inputCounter">Input Counter</label>
          <input id="inputCounter" onChange={this.inputCounter}
           type="text"
           value={this.state.inputText} />
          <p># of characters {this.state.lengthInputCounter} </p>
        </div>
        <ValidationComponent length={this.state.lengthInputCounter} />
        {listOfCharacters}
      </div>
    );
  }
}

export default App;