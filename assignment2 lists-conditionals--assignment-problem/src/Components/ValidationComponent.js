import React from 'react';

const ValidationComponent = (prop) => {
    const maxLength = 5;
    let message = 'Right length';

    if (prop.length < maxLength) {
        message = 'Text too short'
    } else if(prop.length > maxLength) {
        message = 'Text too long'
    }

    return (
        <div>
            <p>{message}</p>
        </div>
    );
}
 
export default ValidationComponent;