import React from 'react';

const CharComponent = (prop) => {

    const style = {
        display: 'inline-block',
        padding: '16px',
        textAlign: 'center',
        margin: '16px',
        border: '1px solid black'
    }

    return (
        <div onClick={prop.click} style={style}>
            <p>{prop.value}</p>
        </div>
    );

}

export default CharComponent;