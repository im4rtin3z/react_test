import React, { useState } from 'react';

import IngredientForm from './IngredientForm';
import IngredientsList from './IngredientList';
import Search from './Search';

const Ingredients = () => {

  const [userIngredients, setUserIngredients] = useState([]);

  const addIngredientHandler = ingredient => {
    setUserIngredients(prevIngredients => [...prevIngredients,
    { id: Math.random().toString(), ...ingredient }
    ]);
  }


  const removeIngredientHandler = (ingredientId) => {
    setUserIngredients(prevIngredients => {
     return  prevIngredients.filter(ingredient => ingredient.id !== ingredientId)
    });
  }

  return (
    <div className="App">
      <IngredientForm onAddIngredient={addIngredientHandler} />

      <section>
        <Search />
        <IngredientsList ingredients={userIngredients} onRemoveItem={(id) => { removeIngredientHandler(id) }} />
      </section>
    </div>
  );
}

export default Ingredients;
