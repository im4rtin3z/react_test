import React, { Component } from 'react';
import Person from './Person/Person'

class Persons extends Component {

    // static getDerivedStateFromProps(props, state) {
    //     console.log('[Persons.js] getDerivedStateFromProps');
    //     return state;
    // }

    componentWillUnmount() {
        console.log('[Persons.js] componentWillUnmount');
    }

    shouldComponentUpdate(nextProps, nextState) {

        if (this.props.persons !== nextProps.persons) {
            console.log('[Persons.js] shouldComponentUpdate TRUE');
            return true;
        } else {
            console.log('[Persons.js] shouldComponentUpdate FALSE');
            return false;
        }

    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        console.log('[Persons.js] getSnapshotBeforeUpdate');
        return { message: 'Snapshot' };
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log('[Persons.js] componentDidUpdate');
        console.log(snapshot);
    }

    render() {
        console.log("[Persons.js] rendering...");
        return this.props.persons.map((person, index) => {
            return <Person
                click={() => this.props.clicked(index)}
                name={person.name}
                age={person.age}
                key={person.id}
                changed={(event) => this.props.changed(event, person.id)}
            />
        });
    }
}

export default Persons;