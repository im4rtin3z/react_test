import React, { useState } from 'react';
import './App.css';
import Person from './Person/Person'

const app = props => {

  const [personsState, setPersonsState] = useState({
    persons: [
      { name: "max31", "age": 31 },
      { name: "max32", "age": 32 },
      { name: "max33", "age": 33 }
    ]
  });

  const [otherState, otherSetState] = useState({ 
    otherState: "some other value" });

  console.log(personsState, otherState);

  const switchNameHandler = () => {
    //DON'T DO THIS this.state.persons[0].name = "imartinez";
    setPersonsState({
      persons: [
        { name: "imartinez31", "age": 31 },
        { name: "imartinez32", "age": 32 },
        { name: "imartinez33", "age": 33 }
      ]
    });
  }

  return (
    <div className="App">
      <h1>React app</h1>
      <p>Test paragraph</p>
      <button onClick={switchNameHandler} >Switch name</button>
      <Person name={personsState.persons[0].name} age={personsState.persons[0].age} />
      <Person name={personsState.persons[1].name} age={personsState.persons[1].age} > My hobbies: racing </Person>
      <Person name={personsState.persons[2].name} age={personsState.persons[2].age} />
    </div>
  );

}

export default app;
